/*	Copyright (C) 2018 Pekka Ristola

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

/*
 * main.cpp
 */

#include <cmath>
#include <cstdio>
#include <getopt.h>

#include <iostream>
#include <random>
#include <string>

#include <xercesc/dom/DOM.hpp>
#include <xercesc/framework/LocalFileFormatTarget.hpp>
#include <xercesc/framework/StdOutFormatTarget.hpp>
#include <xercesc/util/PlatformUtils.hpp>

const char* mathml_ns = "http://www.w3.org/1998/Math/MathML";

using namespace xercesc;

DOMElement* createElem(DOMDocument* doc, const char* name) {
	static XMLCh tempstr[100], tempstr2[100];
	XMLString::transcode(name, tempstr, 99);
	XMLString::transcode(mathml_ns, tempstr2, 99);
	return doc->createElementNS(tempstr2, tempstr);
}

void setAttribute(DOMElement* elem, const char* name, const char* value) {
	static XMLCh tempstr[100], tempstr2[100], tempstr3[100];
	XMLString::transcode(mathml_ns, tempstr, 99);
	XMLString::transcode(name, tempstr2, 99);
	XMLString::transcode(value, tempstr3, 99);
	elem->setAttributeNS(tempstr, tempstr2, tempstr3);
}

void checkParentheses(DOMElement* row, DOMDocument* doc, DOMElement* parent) {
	DOMNode* ns = row->getFirstChild()->getNextSibling();
	if (!ns) {
		parent->appendChild(row);
		return;
	}
	char* sub_op1 = XMLString::transcode(ns->getTextContent());
	if (!strcmp(sub_op1, "+") || !strcmp(sub_op1, "-")) {
		DOMElement* fenc = createElem(doc, "mfenced");
		fenc->appendChild(row);
		parent->appendChild(fenc);
	} else parent->appendChild(row);
	XMLString::release(&sub_op1);
}

static int difficulty_level = 2;
static bool easy = false;

void generateNode(DOMElement* parent, DOMDocument* doc, uint16_t deepness) {
	using std::uniform_int_distribution;
	XMLCh tempstr[100];
	static std::random_device gen("/dev/urandom");
	static uniform_int_distribution<int> nori(1, 6);
	uniform_int_distribution<int> deep(0, std::floor(std::exp(deepness - difficulty_level)));

	if (deep(gen) < 3) {
		static uniform_int_distribution<int> group_dist(1, easy ? 6 : 9);
		int group_num = group_dist(gen);
		if (group_num <= 3) { // basic operations
			static uniform_int_distribution<int> basicop_dist(1, 4); // TODO: Create enum
			int op_num = basicop_dist(gen);
			if (op_num < 3) {
				DOMElement* row1 = createElem(doc, "mrow");
				generateNode(row1, doc, deepness + 1);
				parent->appendChild(row1);
				DOMElement* op = createElem(doc, "mo");
				parent->appendChild(op);
				switch (op_num) {
				case 1:
					XMLString::transcode("+", tempstr, 99);
					break;
				case 2:
					XMLString::transcode("-", tempstr, 99);
					break;
				}
				op->setTextContent(tempstr);
				DOMElement* row2 = createElem(doc, "mrow");
				generateNode(row2, doc, deepness + 1);
				parent->appendChild(row2);
			} else if (op_num == 3) {
				DOMElement* row1 = createElem(doc, "mrow");
				generateNode(row1, doc, deepness + 1);
				checkParentheses(row1, doc, parent);
				DOMElement* op = createElem(doc, "mo");
				XMLString::transcode("·", tempstr, 99);
				op->setTextContent(tempstr);
				parent->appendChild(op);
				DOMElement* row2 = createElem(doc, "mrow");
				generateNode(row2, doc, deepness + 1);
				checkParentheses(row2, doc, parent);
			} else {
				DOMElement* frac = createElem(doc, "mfrac");
				DOMElement* row1 = createElem(doc, "mrow");
				generateNode(row1, doc, deepness + 1);
				frac->appendChild(row1);
				DOMElement* row2 = createElem(doc, "mrow");
				generateNode(row2, doc, deepness + 1);
				frac->appendChild(row2);
				parent->appendChild(frac);
			}
		} else if (group_num == 4 || group_num == 5) {
			static uniform_int_distribution<int> exp_dist(1,9);
			static uniform_int_distribution<int> x_dist(1, 6);
			if (easy || exp_dist(gen) <= 5) {
				DOMElement* sup = createElem(doc, "msup");
				DOMElement* row = createElem(doc, "mrow");
				generateNode(row, doc, deepness + 1);
				char* tagname = XMLString::transcode(row->getFirstElementChild()->getTagName());
				if (strcmp(tagname, "mn") && strcmp(tagname, "mi")) {
					DOMElement* fenc = createElem(doc, "mfenced");
					fenc->appendChild(row);
					sup->appendChild(fenc);
				} else sup->appendChild(row);
				XMLString::release(&tagname);
				DOMElement* exp;
				if (x_dist(gen) == 1) {
					exp = createElem(doc, "mi");
					XMLString::transcode("x", tempstr, 99);
				} else {
					static uniform_int_distribution<int> n_dist(2, 5);
					exp = createElem(doc, "mn");
					XMLString::transcode(std::to_string(n_dist(gen)).c_str(), tempstr, 99);
				}
				exp->setTextContent(tempstr);
				sup->appendChild(exp);
				parent->appendChild(sup);
			} else {
				static uniform_int_distribution<int> n_dist(2, 9);
				DOMElement* sup = createElem(doc, "msup");
				DOMElement* base;
				if (x_dist(gen) == 1) {
					base = createElem(doc, "mi");
					XMLString::transcode("x", tempstr, 99);
				} else {
					base = createElem(doc, "mn");
					XMLString::transcode(std::to_string(n_dist(gen)).c_str(), tempstr, 99);
				}
				base->setTextContent(tempstr);
				sup->appendChild(base);
				DOMElement* row = createElem(doc, "mrow");
				generateNode(row, doc, deepness + 1);
				sup->appendChild(row);
				parent->appendChild(sup);
			}
		} else if (group_num == 6) { // trigonometric functions
			static uniform_int_distribution<int> trig_dist(1, 10);
			int trig_num = trig_dist(gen);
			DOMElement* outrow = createElem(doc, "mrow");
			DOMElement* trigfun = createElem(doc, "mo");
			if (trig_num <= 3)
				XMLString::transcode("sin", tempstr, 99);
			else if (trig_num <= 6)
				XMLString::transcode("cos", tempstr, 99);
			else XMLString::transcode("tan", tempstr, 99);
			trigfun->setTextContent(tempstr);
			outrow->appendChild(trigfun);
			DOMElement* fenc = createElem(doc, "mfenced");
			DOMElement* inrow = createElem(doc, "mrow");
			generateNode(inrow, doc, deepness + 1);
			fenc->appendChild(inrow);
			outrow->appendChild(fenc);
			parent->appendChild(outrow);
		} else if (group_num == 7) { // root
			static uniform_int_distribution<int> root_dist(1, 5);
			if (root_dist(gen) <= 3) { // square root
				DOMElement* sqrt = createElem(doc, "msqrt");
				DOMElement* row = createElem(doc, "mrow");
				generateNode(row, doc, deepness + 1);
				sqrt->appendChild(row);
				parent->appendChild(sqrt);
			} else { // general root
				static uniform_int_distribution<int> n_dist(3, 6);
				DOMElement* root = createElem(doc, "mroot");
				DOMElement* row = createElem(doc, "mrow");
				generateNode(row, doc, deepness + 1);
				root->appendChild(row);
				DOMElement* index = createElem(doc, "mn");
				XMLString::transcode(std::to_string(n_dist(gen)).c_str(), tempstr, 99);
				index->setTextContent(tempstr);
				root->appendChild(index);
				parent->appendChild(root);
			}
		} else if (group_num == 8) { // logarithm
			static uniform_int_distribution<int> log_dist(1, 3);
			DOMElement* outrow = createElem(doc, "mrow");
			if (log_dist(gen) <= 2) { // random-base logarithm
				static uniform_int_distribution<int> n_dist(2, 12);
				DOMElement* sub = createElem(doc, "msub");
				DOMElement* log = createElem(doc, "mo");
				XMLString::transcode("log", tempstr, 99);
				log->setTextContent(tempstr);
				sub->appendChild(log);
				DOMElement* base = createElem(doc, "mn");
				XMLString::transcode(std::to_string(n_dist(gen)).c_str(), tempstr, 99);
				base->setTextContent(tempstr);
				sub->appendChild(base);
				outrow->appendChild(sub);
			} else { // natural logarithm
				DOMElement* ln = createElem(doc, "mo");
				XMLString::transcode("ln", tempstr, 99);
				ln->setTextContent(tempstr);
				outrow->appendChild(ln);
			}
			DOMElement* fenc = createElem(doc, "mfenced");
			setAttribute(fenc, "open", "|");
			setAttribute(fenc, "close", "|");
			DOMElement* inrow = createElem(doc, "mrow");
			generateNode(inrow, doc, deepness + 1);
			fenc->appendChild(inrow);
			outrow->appendChild(fenc);
			parent->appendChild(outrow);
		} else { // inverse trigonometric functions
			static uniform_int_distribution<int> type_dist(1, 3);
			int type_num = type_dist(gen);
			DOMElement* outrow = createElem(doc, "mrow");
			DOMElement* function = createElem(doc, "mo");
			const char* typestr = type_num == 1 ? "arcsin" : type_num == 2 ? "arccos" : "arctan";
			XMLString::transcode(typestr, tempstr, 99);
			function->setTextContent(tempstr);
			outrow->appendChild(function);
			DOMElement* fenc = createElem(doc, "mfenced");
			DOMElement* inrow = createElem(doc, "mrow");
			generateNode(inrow, doc, deepness + 1);
			fenc->appendChild(inrow);
			outrow->appendChild(fenc);
			parent->appendChild(outrow);
		}
	} else if (nori(gen) < 3) {
		static uniform_int_distribution<int> n_dist(1, 9);
		DOMElement* num = createElem(doc, "mn");
		XMLString::transcode(std::to_string(n_dist(gen)).c_str(), tempstr, 99);
		num->setTextContent(tempstr);
		parent->appendChild(num);
	} else {
		DOMElement* num = createElem(doc, "mi");
		XMLString::transcode("x", tempstr, 99);
		num->setTextContent(tempstr);
		parent->appendChild(num);
	}
}

int main(int argc, char** argv) {
	using std::endl;
	int c;
	std::string output_file = "mathout.xml";
	bool human_readable = false;
	while (1) {
		static option long_options[] = {
				{ "help", no_argument, nullptr, 'h' },
				{ "output", required_argument, nullptr, 'o' },
				{ "difficulty", required_argument, nullptr, 'd' },
				{ "human-readable", no_argument, nullptr, 'H' },
				{ nullptr, 0, nullptr, 0 }
		};
		int option_index = 0;
		c = getopt_long(argc, argv, "ho:d:7H", long_options, &option_index);
		if (c == -1) break;
		switch (c) {
		case 'o':
			output_file = optarg;
			break;
		case 'd':
			try {
				difficulty_level = std::stoi(optarg);
			} catch (std::invalid_argument& i) {
				std::cerr << "Difficulty level must be an integer" << endl;
				return 2;
			} catch (std::out_of_range& o) {
				std::cerr << "Difficulty level was out of bounds" << endl;
				return 2;
			}
			break;
		case '7':
			easy = true;
			break;
		case 'h':
			std::cout << "Some help text here" << endl;
			return 0;
			break;
		case 'H':
			human_readable = true;
			break;
		case '?':
			break;
		default:
			return 2;
		}
	}

	try {
		XMLPlatformUtils::Initialize();
	} catch (const XMLException& e) {
		std::cerr << "Failed to initialize xerces-c." << endl;
		return 1;
	}

	XMLCh tempstr[100], tempstr2[100];
	XMLString::transcode("XML 1.0 LS", tempstr, 99);
	DOMImplementation* impl = DOMImplementationRegistry::getDOMImplementation(tempstr);
	XMLString::transcode(mathml_ns, tempstr, 99);
	XMLString::transcode("math", tempstr2, 99);
	DOMDocument* doc = impl->createDocument(tempstr, tempstr2, nullptr);
	DOMElement* root = doc->getDocumentElement();

	generateNode(root, doc, 0);

	DOMLSSerializer* serializer = static_cast<DOMImplementationLS*>(impl)->createLSSerializer();
	if (human_readable) {
		if (serializer->getDomConfig()->canSetParameter(XMLUni::fgDOMWRTFormatPrettyPrint, true))
			serializer->getDomConfig()->setParameter(XMLUni::fgDOMWRTFormatPrettyPrint, true);
		else std::cerr << "Failed to produce human readable output" << endl;
	}
	XMLFormatTarget* formTarget;
	if (output_file == "-")
		formTarget = new StdOutFormatTarget();
	else formTarget = new LocalFileFormatTarget(output_file.c_str());
	DOMLSOutput* theOutput = ((DOMImplementationLS*)impl)->createLSOutput();
	theOutput->setByteStream(formTarget);
	try {
		serializer->write(doc, theOutput);
	} catch (...) {
		std::cerr << "Unexpected Exception" << endl;
		return 1;
	}

	theOutput->release();
	serializer->release();
	delete formTarget;
	doc->release();

	XMLPlatformUtils::Terminate();
	return 0;
}
